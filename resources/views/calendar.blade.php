<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calendario</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <link rel="stylesheet" href="css/app.css">
</head>

<body class="bg-gray-100">
    @php
        // Obtener el número de días en el mes actual
        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y'));
        // Obtener el día actual
        $currentDay = date('j');
        // Obtener el primer día del mes actual (1: domingo, 2: lunes, ..., 7: sábado)
        $firstDayOfMonth = date('N', strtotime(date('Y-m-01')));
        // Nombre del mes actual
        $currentMonthName = date('F');
        // Año actual
        $currentYear = date('Y');
        // Día de la semana actual (1: domingo, 2: lunes, ..., 7: sábado)
        $currentWeekday = date('N');
        // Nombre del día actual
        $currentDayName = date('l');
        // Fecha actual en formato "d de F de Y"
        $currentDate = date('d \d\e F \d\e Y');
    @endphp

    <div class="container mx-auto mt-5">
        <div class="bg-white rounded-lg shadow overflow-hidden">
            <div class="text-center font-semibold text-lg p-4 bg-blue-500 text-white">
                {{ $currentMonthName }} {{ $currentYear }}
            </div>
            <div class="grid grid-cols-7 border-b border-gray-200 text-center">
                @foreach (['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'] as $day)
                    <div class="py-2 font-semibold text-sm border-r border-b border-gray-200 bg-gray-200">
                        {{ $day }}</div>
                @endforeach
            </div>

            @php
                // Calcular el número de celdas vacías al principio del calendario
                $emptyCells = $firstDayOfMonth - 1;
            @endphp

            <div class="grid grid-cols-7 border-b border-r border-gray-200">
                @for ($i = 1; $i <= $emptyCells; $i++)
                    <div class="py-3 border-r border-gray-200"></div>
                @endfor

                @for ($i = 1; $i <= $daysInMonth; $i++)
                    <div class="py-3 text-center border-r border-gray-200 content-center">
                        <div
                            class="@if ($i == $currentDay) m-auto bg-blue-500 text-white rounded-full w-8 h-8 flex items-center justify-center font-semibold @endif">
                            {{ $i }}</div>
                    </div>

                    @if (($i + $emptyCells) % 7 === 0)
            </div>
            @if ($i < $daysInMonth)
                <div class="grid grid-cols-7 border-b border-r border-gray-200">
            @endif
            @endif
            @endfor
        </div>

        <div class="text-center p-4">
            <p class="text-lg font-semibold">{{ $currentDayName }}, {{ $currentDate }}</p>
        </div>
    </div>

    <p class="text-right py-2"> By: Mariano Ismael García Guzmán</p>

</body>

</html>
